import { HttpErrorResponse } from '@angular/common/http';
import { OrdersService } from './../shared/services/orders.services';
import { OrderPosition, Order } from './../shared/interfaces';
import { OrderService } from './order.service';
import { MaterialInstance } from './../shared/classes/material.service';
import { MaterialService } from 'src/app/shared/classes/material.service';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.css']
})
export class OrderPageComponent implements OnInit, OnDestroy, AfterViewInit {

  isRoot: boolean;
  aSub: Subscription;
  @ViewChild('modal') modalRef: ElementRef;
  modal: MaterialInstance;
  pending = false;

  constructor(private router: Router,
              public order: OrderService,
              private ordersService: OrdersService) { }

  ngOnInit() {
    this.isRoot = this.router.url === '/order';
    this.aSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.isRoot = this.router.url === '/order';
      }
    });
  }

  ngOnDestroy() {
    this.modal.destroy();
    if (this.aSub) {this.aSub.unsubscribe(); }
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef);
  }

  open() {
    this.modal.open();
  }

  cancel() {
    this.modal.close();
  }

  submit() {
    this.pending = true;
    const order: Order = {
      list: this.order.list.map(item => {
        delete item._id;
        return item;
      })
    };
    this.ordersService.create(order)
    .then((resp: Order) => {
      MaterialService.toast(`Заказ №${resp.order} был добавлен.`);
      this.order.clear();
    })
    .catch((err: HttpErrorResponse) => {
      MaterialService.toast(err.error.message);
    })
    .finally(() => {
      this.modal.close();
      this.pending = false;
    });
  }

  removePosition(orderPosition: OrderPosition) {
    this.order.remove(orderPosition);
  }

}
