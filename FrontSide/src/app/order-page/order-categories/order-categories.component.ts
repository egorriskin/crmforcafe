import { Category } from './../../shared/interfaces';
import { CategoriesService } from './../../shared/services/categories.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-categories',
  templateUrl: './order-categories.component.html',
  styleUrls: ['./order-categories.component.css']
})
export class OrderCategoriesComponent implements OnInit {

  categories: Promise<Category[]>;

  constructor(private categoriesService: CategoriesService) { }

  ngOnInit() {
    this.categories = this.categoriesService.fetch();
  }

}
