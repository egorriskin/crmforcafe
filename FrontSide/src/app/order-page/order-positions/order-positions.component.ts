import { MaterialService } from 'src/app/shared/classes/material.service';
import { Position } from './../../shared/interfaces';
import { switchMap, map } from 'rxjs/operators';
import { PositionService } from './../../shared/services/position.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-order-positions',
  templateUrl: './order-positions.component.html',
  styleUrls: ['./order-positions.component.css'],
})
export class OrderPositionsComponent implements OnInit {

  positions$: Observable<Position[]>;

  constructor(private route: ActivatedRoute,
              private positionsService: PositionService,
              private orderService: OrderService) {}

  ngOnInit() {
    this.positions$ = this.route.params
    .pipe(
      switchMap((params: Params) => {
        return this.positionsService.fetch(params['id']);
      }),
      map((positions: Position[]) =>{
        return positions.map(pos => {
          pos.quantity = 1;
          return pos;
        });
      })
    );
  }

  addToOrder(position: Position) {
    MaterialService.toast(`Добавлено x${position.quantity}`);
    this.orderService.add(position);
  }
}
