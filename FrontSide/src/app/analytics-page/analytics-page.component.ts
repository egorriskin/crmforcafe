import { MaterialService } from 'src/app/shared/classes/material.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AnalyticsPage } from './../shared/interfaces';
import { AnalyticsService } from './../shared/services/analytics.service';
import { Chart } from 'chart.js';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-analytics-page',
  templateUrl: './analytics-page.component.html',
  styleUrls: ['./analytics-page.component.css']
})
export class AnalyticsPageComponent implements AfterViewInit {

  @ViewChild('gain') gainRef: ElementRef;
  @ViewChild('order') orderRef: ElementRef;

  average: number;
  pending = true;

  constructor(private service: AnalyticsService) { }

  ngAfterViewInit() {
    this.service.getAnalytics()
    .then((resp: AnalyticsPage) => {
      this.average = resp.average;

      this.createGainChart(resp);
      this.createOrderChart(resp);

      this.pending = false;
    })
    .catch((err: HttpErrorResponse) => {
      MaterialService.toast(err.error.message);
    });
  }

  createGainChart(resp: AnalyticsPage) {
    const gainConfig: any = {
      label: 'Выручка',
      color: 'rgb(255, 99, 132)'
    };

    gainConfig.labels = resp.chart.map(item => item.label);
    gainConfig.data = resp.chart.map(item => item.gain);

    const gainContext = this.gainRef.nativeElement.getContext('2d');
    gainContext.canvas.height = '300px';

    new Chart(gainContext, createChartConfig(gainConfig));
  }

  createOrderChart(resp: AnalyticsPage) {
    const orderConfig: any = {
      label: 'Заказы',
      color: 'rgb(54, 462, 235)'
    };

    orderConfig.labels = resp.chart.map(item => item.label);
    orderConfig.data = resp.chart.map(item => item.order);

    const orderContext = this.orderRef.nativeElement.getContext('2d');
    orderContext.canvas.height = '300px';

    new Chart(orderContext, createChartConfig(orderConfig));
  }

}

function createChartConfig({labels, data, label, color}) {
  return {
    type: 'line',
    options: {
      responsive: true
    },
    data: {
      labels,
      datasets: [
        {
          label, data,
          borderColor: color,
          steppedLine: false,
          fill: false
        }
      ]
    }
  };
}
