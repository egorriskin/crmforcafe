import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MaterialInstance } from './../../../shared/classes/material.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Position, Message } from './../../../shared/interfaces';
import { PositionService } from './../../../shared/services/position.service';
import { Component, OnInit, Input, ElementRef, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MaterialService } from 'src/app/shared/classes/material.service';

@Component({
  selector: 'app-positions-form',
  templateUrl: './positions-form.component.html',
  styleUrls: ['./positions-form.component.css']
})
export class PositionsFormComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input('categoryId') categoryId: string;
  @ViewChild('modal') modalRef: ElementRef;

  positions: Position[] = [];
  positionId = null;
  loading = false;
  modal: MaterialInstance;
  form: FormGroup;

  constructor(private positionService: PositionService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      cost: new FormControl(1, [Validators.required, Validators.min(1)])
    });

    this.loading = true;
    this.positionService.fetch(this.categoryId)
    .then((resp: Position[]) => {
      this.positions = resp;
      this.loading = false;
    })
    .catch((err: HttpErrorResponse) => {
      MaterialService.toast(err.error.message);
    });
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef);
  }

  ngOnDestroy() {
    this.modal.destroy();
  }

  onSelectPosition(position: Position) {
    this.positionId = position._id;
    this.form.patchValue({
      name: position.name,
      cost: position.cost
    });
    this.modal.open();
    MaterialService.updateTextInputs();
  }

  onAddPosition() {
    this.positionId = null;
    this.form.reset({
      name: null,
      cost: 1
    });
    this.modal.open();
    MaterialService.updateTextInputs();
  }

  onCancel() {
    this.modal.close();
  }

  onSubmit() {
    this.form.disable();

    const newPosition: Position = {
      name: this.form.value.name,
      cost: this.form.value.cost,
      category: this.categoryId
    };

    const completed = () => {
      this.modal.close();
      this.form.reset({name: '', cost: 1});
      this.form.enable();
    };

    if (this.positionId) {
      newPosition._id = this.positionId;
      this.positionService.update(newPosition)
      .then((resp: Position) => {
        const index = this.positions.findIndex(p => p._id === resp._id);
        this.positions[index] = resp;
        MaterialService.toast('Изменения сохранены');
      })
      .catch((err: HttpErrorResponse) => {
        MaterialService.toast(err.error.message);
      })
      .finally(() => {
        completed();
      });
    } else {
      this.positionService.create(newPosition)
      .then((resp: Position) => {
        MaterialService.toast('Позиция создана');
        this.positions.push(resp);
      })
      .catch((err: HttpErrorResponse) => {
        MaterialService.toast(err.error.message);
      })
      .finally(() => {
        completed();
      });
    }
  }

  onDeletePosition(event: Event, position: Position) {
    event.stopPropagation();
    const decision = confirm(`Удалить позицию ${position.name}?`);

    if (decision) {
      this.positionService.delete(position)
      .then((resp: Message) => {
        const index = this.positions.findIndex(p => p._id === position._id);
        this.positions.splice(index, 1);
        MaterialService.toast(resp.message);
      })
      .catch((err: HttpErrorResponse) => {
        MaterialService.toast(err.error.message);
      });
    }
  }

}
