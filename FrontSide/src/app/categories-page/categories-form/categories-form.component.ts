import { Category, Message } from './../../shared/interfaces';
import { MaterialService } from './../../shared/classes/material.service';
import { HttpErrorResponse } from '@angular/common/http';
import { CategoriesService } from './../../shared/services/categories.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { of, Subscription } from 'rxjs';

@Component({
  selector: 'app-categories-form',
  templateUrl: './categories-form.component.html',
  styleUrls: ['./categories-form.component.css']
})
export class CategoriesFormComponent implements OnInit, OnDestroy {

  isNew = true;
  form: FormGroup;
  @ViewChild('inputFile') inputFileRef: ElementRef;
  image: File;
  imagePreview: any = '';
  category: Category;
  aSub: Subscription;

  constructor(private route: ActivatedRoute,
              private categoriesService: CategoriesService,
              private router: Router) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required)
    });

    this.form.disable();

    this.aSub = this.route.params
    .pipe(
      switchMap(
        (params: Params) => {
          if (params['id']) {
            this.isNew = false;
            return this.categoriesService.getById(params['id']);
          }
          return of(null);
        }
      )
    )
    .subscribe(
      (resp: Category) => {
        if (resp) {
          this.category = resp;
          this.form.patchValue({
            name: resp.name
          });
          this.imagePreview = resp.imageSrc;
          MaterialService.updateTextInputs();
        }
        this.form.enable();
      },
      (err: HttpErrorResponse) => {
        MaterialService.toast(err.error.message);
      }
    );
  }

  ngOnDestroy(): void {
    if (this.aSub) {
      this.aSub.unsubscribe();
    }
  }

  triggerClick() {
    this.inputFileRef.nativeElement.click();
  }

  deleteCategory() {
    const decision = confirm(`Вы уверены, что хотите удалить категорию ${this.category.name}`);

    if (decision) {
      this.categoriesService.delete(this.category._id)
      .then((resp: Message) => {
        this.router.navigate(['/categories']);
        MaterialService.toast(resp.message);
      })
      .catch((err: HttpErrorResponse) => {
        MaterialService.toast(err.error.message);
      });
    }
  }

  onFileUpload(event: any) {
    const file = event.target.files[0];
    this.image = file;

    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result;
    };
    reader.readAsDataURL(file);
  }

  onSubmit() {
    let obs: Promise<Category>;
    this.form.disable();
    if (this.isNew) {
      obs = this.categoriesService.create(this.form.value.name, this.image);
    } else {
      obs = this.categoriesService.update(this.category._id, this.form.value.name, this.image);
    }
    obs
    .then((resp: Category) => {
      this.form.enable();
      MaterialService.toast('Изменения сохранены');
      this.category = resp;
    })
    .catch((err: HttpErrorResponse) => {
      this.form.enable();
      MaterialService.toast(err.error.message);
    });
  }

}
