import { OverviewPage, AnalyticsPage } from './../interfaces';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class AnalyticsService {

    constructor(private http: HttpClient) {}

    getOverview() {
        return this.http.get<OverviewPage>('/api/analytics/overview').toPromise();
    }

    getAnalytics() {
        return this.http.get<AnalyticsPage>('/api/analytics/analytics').toPromise();
    }

}
