import { Order } from './../interfaces';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class OrdersService {

    constructor(private http: HttpClient) {}

    create(order: Order) {
        return this.http.post<Order>('/api/order', order).toPromise();
    }

    fetch(query: any = {}) {
        const options = {
            params: new HttpParams({
                fromObject: query
            })
        };

        return this.http.get<Order[]>('/api/order', options).toPromise();
    }
}
