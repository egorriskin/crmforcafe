import { IUser } from './../interfaces';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private token = null;

    constructor(private http: HttpClient) {}

    register(user: IUser) {
        return this.http.post('/api/auth/register', user).toPromise();
    }

    login(user: IUser) {
        return this.http.post('/api/auth/login', user).toPromise()
        .then((resp: any) => {
            localStorage.setItem('auth-token', resp.token);
            this.setToken(resp.token);
        });
    }

    setToken(token: string) {
        this.token = token;
    }

    getToken(): string {
        return this.token;
    }

    isAuthenticated(): boolean {
        return !!this.token;      // !! - перевод к булеву
    }

    logOut() {
        this.setToken(null);
        localStorage.clear();
    }

}
