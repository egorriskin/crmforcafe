import { Position, Message } from './../interfaces';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class PositionService {

    constructor(private http: HttpClient) {}

    fetch(categoryId) {
        return this.http.get<Position[]>('/api/position/' + categoryId).toPromise();
    }

    create(position: Position) {
        return this.http.post<Position>('/api/position', position).toPromise();
    }

    update(position: Position) {
        return this.http.patch<Position>('/api/position/' + position._id, position).toPromise();
    }

    delete(position: Position) {
        return this.http.delete<Message>('/api/position/' + position._id).toPromise();
    }

}
