import { Category, Message } from './../interfaces';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class CategoriesService {

    constructor(private http: HttpClient) {}

    fetch() {
        return this.http.get<Category[]>('/api/category').toPromise();
    }

    getById(id: string) {
        return this.http.get<Category>('/api/category/' + id).toPromise();
    }

    create(name: string, image?: File) {
        const fd = new FormData();
        if (image) {
            fd.append('image', image, image.name);
        }
        fd.append('name', name);

        return this.http.post<Category>('/api/category', fd).toPromise();
    }

    update(id: string, name: string, image?: File) {
        const fd = new FormData();
        if (image) {
            fd.append('image', image, image.name);
        }
        fd.append('name', name);

        return this.http.patch<Category>('/api/category/' + id, fd).toPromise();
    }

    delete(id: string) {
        return this.http.delete<Message>('/api/category/' + id).toPromise();
    }

}
