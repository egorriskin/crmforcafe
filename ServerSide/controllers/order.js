const Order = require('../models/Order');
const errorHandler = require('../utils/errorHandler');


module.exports.getAll = async (req, res) => {
    const query = {
        user: req.user.id
    };

    // дата старта
    if (req.query.start) {
        query.date = {
            // Больше или равно
            $gte: req.query.start
        };
    }
    
    // дата окончания
    if (req.query.end) {
        if (!query.date) {
            query.date = {};
        }
        // меньше или равно
        query.date['$lte'] = req.query.end;
    }

    if (req.query.order) {
        query.order = +req.query.order;
    }
    
    try {
        const orders = await Order
        .find(query)
        .sort({date: -1})
        .skip(+req.query.offset) // для пагинации
        .limit(+req.query.limit);
        console.log(query);
        
            
        res.status(200).json(orders);
    } catch (e) {
        errorHandler(res, e);
    }
};

module.exports.create = async (req, res) => {
    try {
        const lastOrder = await Order
            .findOne({user: req.user.id})  // вытащить 1 запись
            .sort({date: -1});             // при сортировке по убыванию даты

        const maxOrder = lastOrder ? lastOrder.order : 0

        const order = await new Order({
            list: req.body.list,
            user: req.user.id,
            order: maxOrder + 1
        }).save();
        
        res.status(201).json(order);
    } catch (e) {
        errorHandler(res, e);
    }
};