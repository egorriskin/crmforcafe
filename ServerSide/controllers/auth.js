const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');
const errorHandler = require('../utils/errorHandler');

module.exports.login = async (req, res) => {
    const candidate = await User.findOne({email: req.body.email});
    if (candidate) {
        // пользователь есть, проверяем пароль
        const passwordResult = bcrypt.compareSync(req.body.password, candidate.password);
        if (passwordResult) {
            // Generate token, password compared
            const token = jwt.sign({
                email: candidate.email,
                userId: candidate._id
            }, keys.jwtSecret, {expiresIn: 3600});
            res.status(200).json({
                token: `Bearer ${token}`
            });
        } else {
            // Пароли не совпали
            res.status(401).json({
                message: 'Неверный пароль'
            });
        }
    } else {
        // пользователя нет -ошибка
        res.status(404).json({
            message: 'Пользователя с таким email нет'
        });
    }
};

module.exports.register = async (req, res) => {
    const candidate = await User.findOne({email: req.body.email});
    if (candidate) {
        // пользователь есть - ошибка
        res.status(409).json({
            message: 'Такой email уже занят, попробуй другой'
        });
    } else {
        // создаем пользователя
        const salt = bcrypt.genSaltSync(10);
        const password = req.body.password;
        const user = new User({
            email: req.body.email,
            password: bcrypt.hashSync(password, salt)
        });
        try {
            await user.save();
            res.status(201).json(user);
        } catch(e) {
            errorHandler(res, e);
        }
    }
};